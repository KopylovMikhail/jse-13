package ru.kopylov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;

import java.util.List;

public class UserEndpointTest {

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @Before
    public void setUp() {
        try {
            adminToken = userEndpoint.loginUser("test", "test");
        } catch (Exception_Exception e) {
            e.printStackTrace();
        }
        try {
            userEndpoint.persistUser("testUser", "testUser");
            userToken = userEndpoint.loginUser("testUser", "testUser");
        } catch (Exception_Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception_Exception {
        @Nullable final User user = userEndpoint.getUserProfile(userToken);
        @Nullable final User admin = userEndpoint.getUserProfile(adminToken);
        userEndpoint.removeUser(adminToken, user.getId());
        userEndpoint.removeSession(adminToken, admin.getId());
    }

    @Test(expected = Exception_Exception.class)
    public void updateUserByGuest() throws Exception_Exception {
        userEndpoint.updateUser(null, "testUser01", "testUser");
    }

    @Test
    public void updateUser() throws Exception_Exception {
        userEndpoint.updateUser(userToken, "testUser01", "testUser");
        @Nullable User user = userEndpoint.getUserProfile(userToken);
        Assert.assertEquals(user.login, "testUser01");

        userEndpoint.updateUser(userToken, "testUser", "testUser");
        user = userEndpoint.getUserProfile(userToken);
        Assert.assertEquals(user.login, "testUser");
    }

    @Test(expected = Exception_Exception.class)
    public void getUserProfileByGuest() throws Exception_Exception {
        @Nullable final User guest = userEndpoint.getUserProfile(null);
    }

    @Test
    public void getUserProfileByUser() throws Exception_Exception {
        @Nullable final User user = userEndpoint.getUserProfile(userToken);
        Assert.assertNotNull(user);
    }

    @Test(expected = Exception_Exception.class)
    public void logoutUser() throws Exception_Exception {
        @Nullable final User user = userEndpoint.getUserProfile(userToken);
        Assert.assertEquals(user.login, "testUser");

        userEndpoint.logoutUser(userToken);
        try {
            userEndpoint.getUserProfile(userToken);
        } catch (Exception_Exception e) { //снова логинимся под testUser'ом, чтобы не валилась ошибка при выполнении @AfterClass
            userToken = userEndpoint.loginUser("testUser", "testUser");
            throw new Exception_Exception();
        }
    }

    @Test
    public void loginUser() throws Exception_Exception {
        Assert.assertNull(userEndpoint.loginUser("dummy", "dummy"));
        Assert.assertNotNull(userToken);
    }

    @Test
    public void persistUser() throws Exception_Exception {
        boolean persistResult = userEndpoint.persistUser("testUser", "testUser");
        Assert.assertFalse(persistResult);

        persistResult = userEndpoint.persistUser("testTest", "testTest");
        Assert.assertTrue(persistResult);
        @NotNull final String testToken = userEndpoint.loginUser("testTest", "testTest");
        @NotNull final User testUser = userEndpoint.getUserProfile(testToken);
        userEndpoint.removeUser(adminToken, testUser.getId());
    }

    @Test(expected = Exception_Exception.class)
    public void removeUserByUser() throws Exception_Exception {
        @Nullable final User user = userEndpoint.getUserProfile(userToken);
        userEndpoint.removeUser(userToken, user.getId());
    }

    @Test
    public void removeUserByAdmin() throws Exception_Exception {
        userEndpoint.persistUser("testTest", "testTest");
        @Nullable final String testToken = userEndpoint.loginUser("testTest", "testTest");
        Assert.assertNotNull(testToken);

        @Nullable final User user = userEndpoint.getUserProfile(testToken);
        userEndpoint.removeUser(adminToken, user.getId());
        Assert.assertNull(userEndpoint.loginUser("testTest", "testTest"));
    }

    @Test(expected = Exception_Exception.class)
    public void getUserListByUser() throws Exception_Exception {
        @NotNull final List<User> users = userEndpoint.getUserList(userToken);
    }

    @Test
    public void getUserListByAdmin() throws Exception_Exception {
        @Nullable final User user = userEndpoint.getUserProfile(userToken);
        @Nullable final User admin = userEndpoint.getUserProfile(adminToken);
        @NotNull final List<User> users = userEndpoint.getUserList(adminToken);
        boolean userPresent = false;
        boolean adminPresent = false;
        Assert.assertTrue(users.size() > 0);

        for (User usr : users) {
            if (usr.getId().equals(user.getId())) userPresent = true;
            if (usr.getId().equals(admin.getId())) adminPresent = true;
        }
        Assert.assertTrue(userPresent);
        Assert.assertTrue(adminPresent);
    }

    @Test
    public void removeSession() throws Exception_Exception {
        userEndpoint.persistUser("testTest", "testTest");
        @Nullable final String testToken = userEndpoint.loginUser("testTest", "testTest");
        @Nullable final User user = userEndpoint.getUserProfile(testToken);
        Assert.assertFalse(userEndpoint.removeSession(adminToken, ""));
        Assert.assertTrue(userEndpoint.removeSession(adminToken, user.getId()));
        userEndpoint.removeUser(adminToken, user.getId());
    }

}