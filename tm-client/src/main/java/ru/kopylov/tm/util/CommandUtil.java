package ru.kopylov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.endpoint.Project;
import ru.kopylov.tm.endpoint.Task;

import java.util.List;

public final class CommandUtil {

    public static void printProjectList(@NotNull List<Project> projectList) {
        System.out.println("[PROJECT LIST]");
        int count = 1;
        for (@NotNull final Project project : projectList) {
            System.out.println(count++ + ". " + project.getName());
        }
    }

    public static void printTaskList(@NotNull List<Task> taskList) {
        System.out.println("[TASK LIST]");
        int count = 1;
        for (final Task task : taskList) {
            System.out.println(count++ + ". " + task.getName());
        }
    }

    public static void printProjectListWithParam(@NotNull List<Project> projectList) {
        System.out.println("[PROJECT LIST]");
        int count = 1;
        for (@NotNull final Project project : projectList) {
            System.out.println(count++ + ". " + project.getName() +
                    "; description: " + project.getDescription() +
                    "; date start: " + DateUtil.xgcToString(project.getDateStart()) +
                    "; date finish: " + DateUtil.xgcToString(project.getDateFinish()) +
                    "; state: " + project.getState());
        }
    }

    public static void printTaskListWithParam(@NotNull List<Task> taskList) {
        System.out.println("[TASK LIST]");
        int count = 1;
        for (final Task task : taskList) {
            System.out.println(count++ + ". " + task.getName() +
                    "; description: " + task.getDescription() +
                    "; date start: " + DateUtil.xgcToString(task.getDateStart()) +
                    "; date finish: " + DateUtil.xgcToString(task.getDateFinish()) +
                    "; state: " + task.getState());
        }
    }

}
