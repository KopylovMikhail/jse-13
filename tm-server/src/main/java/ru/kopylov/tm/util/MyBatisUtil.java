package ru.kopylov.tm.util;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IProjectRepository;
import ru.kopylov.tm.api.repository.ISessionRepository;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.api.repository.IUserRepository;

import javax.sql.DataSource;

public final class MyBatisUtil {

    public static SqlSessionFactory getSqlSessionFactory(
            @Nullable final String user,
            @Nullable final String password,
            @Nullable final String url,
            @Nullable final String driver
    ) {
        final DataSource dataSource =
                new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
