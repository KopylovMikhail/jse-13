package ru.kopylov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.endpoint.IUserEndpoint;
import ru.kopylov.tm.api.service.ISessionService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@WebService
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private final String url = primaryUrl + this.getClass().getSimpleName() + "?wsdl";

    public UserEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final ServiceLocator bootstrap) {
        super(sessionService, bootstrap);
    }

    @WebMethod
    public boolean persistUser(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
        return bootstrap.getUserService().persist(login, password);
    }

    @Nullable
    @WebMethod
    public String loginUser(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
        @Nullable final User loggedUser = bootstrap.getUserService().findOne(login, password);
        @Nullable final Session session = bootstrap.getSessionService().persist(loggedUser);
        return bootstrap.getSessionService().encryptToken(session);
    }

    @WebMethod
    public void logoutUser(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        bootstrap.getSessionService().validate(session);
        bootstrap.getSessionService().remove(session.getId());
    }

    @Nullable
    @WebMethod
    public User getUserProfile(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        bootstrap.getSessionService().validate(session);
        return bootstrap.getUserService().findOne(session.getUserId());
    }

    @WebMethod
    public void updateUser(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        bootstrap.getSessionService().validate(session);
        bootstrap.getUserService().merge(session.getUserId(), login, password);
    }

    @WebMethod
    public void removeUser(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "id") @NotNull final String id
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        validate(session);
        bootstrap.getUserService().remove(id);
    }

    @NotNull
    @WebMethod
    public List<User> getUserList(
            @WebParam(name = "token") @Nullable final String token
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        validate(session);
        return bootstrap.getUserService().findAll();
    }

    @WebMethod
    public boolean removeSession(
            @WebParam(name = "token") @Nullable final String token,
            @WebParam(name = "userId") @NotNull final String userId
    ) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().decryptToken(token);
        validate(session);
        return bootstrap.getSessionService().removeByUserId(userId);
    }

    private void validate(@Nullable Session session) throws Exception {
        bootstrap.getSessionService().validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable User user = bootstrap.getUserService().findOne(userId);
        if (user == null || user.getRole() != TypeRole.ADMIN) throw new Exception("User does not have enough rights.");
    }

}
