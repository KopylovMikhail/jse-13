package ru.kopylov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.api.service.ISessionService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.context.Bootstrap;

@Getter
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    protected ISessionService sessionService;

    @NotNull
    protected ServiceLocator bootstrap = new Bootstrap();

    @NotNull
    protected final String primaryUrl = "http://"
            + bootstrap.getPropertyService().getHost()
            + ":" + bootstrap.getPropertyService().getPort() + "/";

    public AbstractEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final ServiceLocator bootstrap
    ) {
        this.sessionService = sessionService;
        this.bootstrap = bootstrap;
    }

}
