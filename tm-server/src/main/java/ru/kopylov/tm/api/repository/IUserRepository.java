package ru.kopylov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @Update("INSERT INTO taskmanager.app_user (id, login, passwordHash, role) \n" +
            "VALUES (#{id}, #{login}, #{password}, #{role})\n" +
            "ON DUPLICATE KEY UPDATE\n" +
            "id = VALUES(id), login = VALUES(login), passwordHash = VALUES(passwordHash), role = VALUES(role)")
    void merge(@NotNull User user);

    @Insert("INSERT INTO taskmanager.app_user (id, login, passwordHash, role) VALUES (#{id}, #{login}, #{password}, #{role})")
    void persist(@NotNull User user);

    @Select("SELECT * FROM taskmanager.app_user WHERE id = #{id}")
    @Result(property = "password", column = "passwordHash")
    User findOne(@NotNull String id);

    @Delete("DELETE FROM taskmanager.app_user WHERE id = #{id}")
    void remove(@NotNull String id);

    @Select("SELECT * FROM taskmanager.app_user")
    @Result(property = "password", column = "passwordHash")
    List<User> findAll();

}
