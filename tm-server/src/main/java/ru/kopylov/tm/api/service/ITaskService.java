package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Task;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface ITaskService {

    boolean persist(@Nullable Task task) throws Exception;

    boolean persist(@Nullable String currentUserId, @Nullable String taskName) throws Exception;

    @NotNull
    List<Task> findAll() throws Exception;

    @NotNull
    List<Task> findAll(@Nullable String currentUserId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String currentUserId, @Nullable String typeSort) throws Exception;

    boolean merge(@Nullable Task task) throws Exception;

    boolean merge(
            @Nullable String currentUserId,
            @NotNull Integer taskNumber,
            @Nullable String taskName,
            @Nullable String taskDescription,
            @Nullable String taskDateStart,
            @Nullable String taskDateFinish,
            @Nullable Integer stateNumber
    ) throws Exception;

    boolean remove(@Nullable String taskId) throws Exception;

    boolean remove(@Nullable String currentUserId, @NotNull Integer taskNumber) throws Exception;

    void removeAll() throws Exception;

    void removeAll(@Nullable String currentUserId) throws Exception;

    @NotNull
    List<Task> findByContent(@Nullable String content) throws Exception;

}
