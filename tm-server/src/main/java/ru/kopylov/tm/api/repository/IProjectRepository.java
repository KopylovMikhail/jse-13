package ru.kopylov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO taskmanager.app_project (id, name, description, dateStart, dateFinish, user_id, state) " +
            "VALUES (#{id}, #{name}, #{description}, #{dateStart}, #{dateFinish}, #{userId}, #{state})")
    void persist(@NotNull Project project);

//    @Update("UPDATE taskmanager.app_project SET name = #{name}, description = #{description}, dateStart = #{dateStart}, " +
//            "dateFinish = #{dateFinish}, user_id = #{userId}, state = #{state} WHERE id = #{id}")
    @Update("INSERT INTO taskmanager.app_project (id, name, description, dateStart, dateFinish, user_id, state) \n" +
            "VALUES (#{id}, #{name}, #{description}, #{dateStart}, #{dateFinish}, #{userId}, #{state})\n" +
            "ON DUPLICATE KEY UPDATE\n" +
            "id = VALUES(id), name = VALUES(name), description = VALUES(description), dateStart = VALUES(dateStart),\n" +
            "dateFinish = VALUES(dateFinish), user_id = VALUES(user_id), state = VALUES(state)")
    void merge(@NotNull Project project);

    @Select("SELECT * FROM taskmanager.app_project")
    @Result(property = "userId", column = "user_id")
    List<Project> findAll();

    @Select("SELECT * FROM taskmanager.app_project WHERE user_id = #{currentUserId}")
    @Result(property = "userId", column = "user_id")
    List<Project> findAllByUserId(@NotNull String currentUserId);

    @Delete("DELETE FROM taskmanager.app_project WHERE id = #{projectId}")
    void remove(@NotNull String projectId);

    @Delete("DELETE FROM taskmanager.app_project")
    void removeAll();

    @Delete("DELETE FROM taskmanager.app_project WHERE user_id = #{currentUserId}")
    void removeAllByUserId(@NotNull String currentUserId);

    @Select("SELECT * FROM taskmanager.app_project WHERE id = #{projectId}")
    @Result(property = "userId", column = "user_id")
    Project findOne(@NotNull String projectId);

    @Select("SELECT * FROM taskmanager.app_project WHERE user_id = #{currentUserId} AND id = #{projectId}")
    @Result(property = "userId", column = "user_id")
    Project findOneByUserId(@NotNull String currentUserId, @NotNull String projectId);

    @Select("SELECT * FROM taskmanager.app_project WHERE name " +
            "LIKE '%${content, jdbcType=VARCHAR}%' OR description LIKE '%${content, jdbcType=VARCHAR}%'")
    @Result(property = "userId", column = "user_id")
    List<Project> findByContent(@NotNull String content);

}
