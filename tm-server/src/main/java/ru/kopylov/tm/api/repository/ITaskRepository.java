package ru.kopylov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    @Update("INSERT INTO taskmanager.app_task (id, name, description, dateStart, dateFinish, project_id, user_id, state) \n" +
            "VALUES (#{id}, #{name}, #{description}, #{dateStart}, #{dateFinish}, #{projectId}, #{userId}, #{state})\n" +
            "ON DUPLICATE KEY UPDATE\n" +
            "id = VALUES(id), name = VALUES(name), description = VALUES(description), dateStart = VALUES(dateStart),\n" +
            "dateFinish = VALUES(dateFinish), project_id = VALUES(project_id), user_id = VALUES(user_id), state = VALUES(state)")
    void merge(@NotNull Task task);

    @Insert("INSERT INTO taskmanager.app_task (id, name, description, dateStart, dateFinish, project_id, user_id, state) " +
            "VALUES (#{id}, #{name}, #{description}, #{dateStart}, #{dateFinish}, #{projectId}, #{userId}, #{state})")
    void persist(@NotNull Task task);

    @Select("SELECT * FROM taskmanager.app_task")
    @Result(property = "projectId", column = "project_id")
    @Result(property = "userId", column = "user_id")
    List<Task> findAll();

    @Select("SELECT * FROM taskmanager.app_task WHERE user_id = #{currentUserId}")
    @Result(property = "projectId", column = "project_id")
    @Result(property = "userId", column = "user_id")
    List<Task> findAllByUserId(@Param("currentUserId") @NotNull String currentUserId);

    @Delete("DELETE FROM taskmanager.app_task WHERE id = #{taskId}")
    void remove(@Param("taskId") @NotNull String taskId);

    @Delete("DELETE FROM taskmanager.app_task")
    void removeAll();

    @Delete("DELETE FROM taskmanager.app_task WHERE user_id = #{currentUserId}")
    void removeAllByUserId(@Param("currentUserId") @NotNull String currentUserId);

    @Select("SELECT * FROM taskmanager.app_task WHERE id = #{taskId}")
    @Result(property = "projectId", column = "project_id")
    @Result(property = "userId", column = "user_id")
    Task findOne(@Param("taskId") @NotNull String taskId);

    @Select("SELECT * FROM taskmanager.app_task WHERE project_id = #{projectId}")
    @Result(property = "projectId", column = "project_id")
    @Result(property = "userId", column = "user_id")
    List<Task> findAllByProjectId(@Param("projectId") @NotNull String projectId);

    @Select("SELECT * FROM taskmanager.app_task WHERE name " +
            "LIKE '%${content, jdbcType=VARCHAR}%' OR description LIKE '%${content, jdbcType=VARCHAR}%'")
    @Result(property = "projectId", column = "project_id")
    @Result(property = "userId", column = "user_id")
    List<Task> findByContent(@NotNull String content);

    @Update("UPDATE taskmanager.app_task SET project_id = #{projectId} WHERE id = #{taskId} AND user_id = #{currentUserId}")
    void setProjectId(
            @Param("currentUserId") @NotNull String currentUserId,
            @Param("projectId") @NotNull String projectId,
            @Param("taskId") @NotNull String taskId
    );

}
