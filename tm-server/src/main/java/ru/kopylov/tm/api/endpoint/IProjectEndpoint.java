package ru.kopylov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.enumerated.State;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @NotNull
    String getUrl();

    @WebMethod
    void clearProject(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    boolean createProject(
            @WebParam(name = "token") @Nullable String token,
            @WebParam(name = "projectName") @NotNull String projectName
    ) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findProjectContent(
            @WebParam(name = "token") @Nullable String token,
            @WebParam(name = "findWord") @NotNull String findWord
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Project> getProjectList(
            @WebParam(name = "token") @Nullable String token,
            @WebParam(name = "typeSort") @Nullable String typeSort
    ) throws Exception;

    @WebMethod
    boolean removeProject(
            @WebParam(name = "token") @Nullable String token,
            @WebParam(name = "projectNumber") @NotNull Integer projectNumber
    ) throws Exception;

    @WebMethod
    boolean setProjectTask(
            @WebParam(name = "token") @Nullable String token,
            @WebParam(name = "projectNumber") @NotNull Integer projectNumber,
            @WebParam(name = "taskNumber") @NotNull Integer taskNumber
    ) throws Exception;

    @NotNull
    @WebMethod
    List<Task> getProjectTaskList(
            @WebParam(name = "token") @Nullable String token,
            @WebParam(name = "projectNumber") @NotNull Integer projectNumber
    ) throws Exception;

    @WebMethod
    boolean updateProject(
            @WebParam(name = "token") @Nullable String token,
            @WebParam(name = "projectNumber") @NotNull Integer projectNumber,
            @WebParam(name = "projectName") @Nullable String projectName,
            @WebParam(name = "projectDescription") @Nullable String projectDescription,
            @WebParam(name = "projectDateStart") @Nullable String projectDateStart,
            @WebParam(name = "projectDateFinish") @Nullable String projectDateFinish,
            @WebParam(name = "stateNumber") @NotNull Integer stateNumber
    ) throws Exception;

    @NotNull
    @WebMethod
    State[] getStateList();

}
