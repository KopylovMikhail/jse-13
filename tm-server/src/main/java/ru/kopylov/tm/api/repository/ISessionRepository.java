package ru.kopylov.tm.api.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.Session;

public interface ISessionRepository {

    @Delete("DELETE FROM taskmanager.app_session WHERE id = #{sessionId}")
    void remove(@NotNull String sessionId);

    @Insert("INSERT INTO taskmanager.app_session (id, timestamp , user_id, signature) " +
            "VALUES (#{id}, #{timestamp}, #{userId}, #{signature})")
    void persist(@NotNull Session session);

    @Select("SELECT * FROM taskmanager.app_session WHERE id = #{sessionId}")
    @Result(property = "userId", column = "user_id")
    Session findOne(@NotNull String sessionId);

    @Delete("DELETE FROM taskmanager.app_session WHERE user_id = #{userId}")
    void removeByUserId(@NotNull String userId);

}
