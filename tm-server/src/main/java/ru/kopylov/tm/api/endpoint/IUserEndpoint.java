package ru.kopylov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @NotNull
    String getUrl();

    @WebMethod
    boolean persistUser(
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

    @Nullable
    @WebMethod
    String loginUser(
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    void logoutUser(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @Nullable
    @WebMethod
    public User getUserProfile(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

    @WebMethod
    void updateUser(
            @WebParam(name = "token") @Nullable String token,
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    void removeUser(
            @WebParam(name = "token") @Nullable String token,
            @WebParam(name = "id") @NotNull String id
    ) throws Exception;

    @NotNull
    @WebMethod
    List<User> getUserList(
            @WebParam(name = "token") @Nullable String token
    ) throws Exception;

}
