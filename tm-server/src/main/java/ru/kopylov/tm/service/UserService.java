package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IUserRepository;
import ru.kopylov.tm.api.service.IUserService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.util.HashUtil;

import java.util.Collections;
import java.util.List;

@NoArgsConstructor
public final class UserService extends AbstractService implements IUserService {

    @Nullable
    private IUserRepository userMapper;

    public UserService(@NotNull ServiceLocator bootstrap) {
        super(bootstrap);
    }

    public boolean merge(@Nullable final User user) {
        if (user == null) return false;
        if (user.getId() == null || user.getId().isEmpty()) return false;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return false;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            userMapper = sqlSession.getMapper(IUserRepository.class);
            userMapper.merge(user);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    public boolean merge(
            @NotNull final String currentUserId,
            @NotNull final String login,
            @NotNull final String password
    ) {
        @Nullable final User user = findOne(currentUserId);
        if (user == null) return false;
        @NotNull final String hashPassword = HashUtil.hash(password);
        user.setLogin(login);
        user.setPassword(hashPassword);
        return merge(user);
    }

    public boolean persist(@Nullable final User user) {
        if (user == null) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            userMapper = sqlSession.getMapper(IUserRepository.class);
            userMapper.persist(user);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    public boolean persist(@NotNull final String login, @NotNull final String password) {
        if (password.isEmpty() || login.isEmpty()) return false;
        @NotNull final List<User> userList;
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();) {
            userMapper = sqlSession.getMapper(IUserRepository.class);
            userList = userMapper.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        for (@NotNull final User usr : userList) {
            if (login.equals(usr.getLogin())) return false;
        }
        @NotNull final String hashPassword = HashUtil.hash(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassword(hashPassword);
        user.setRole(TypeRole.USER);
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            userMapper = sqlSession.getMapper(IUserRepository.class);
            userMapper.persist(user);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }

    }

    @Nullable
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();) {
            userMapper = sqlSession.getMapper(IUserRepository.class);
            return userMapper.findOne(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public User findOne(@NotNull final String login, @NotNull final String password) {
        if (password.isEmpty() || login.isEmpty()) return null;
        @NotNull final String hashPassword = HashUtil.hash(password);
        @Nullable User loggedUser = null;
        @NotNull final List<User> userList = findAll();
        for (@NotNull final User user : userList) {
            if (login.equals(user.getLogin()) && hashPassword.equals(user.getPassword())) {
                loggedUser = user;
                break;
            }
        }
        return loggedUser;
    }

    @NotNull
    public List<User> findAll() {
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();) {
            userMapper = sqlSession.getMapper(IUserRepository.class);
            return userMapper.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            userMapper = sqlSession.getMapper(IUserRepository.class);
            userMapper.remove(id);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

}
