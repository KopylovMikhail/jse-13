package ru.kopylov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.ISessionRepository;
import ru.kopylov.tm.api.service.IPropertyService;
import ru.kopylov.tm.api.service.ISessionService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Session;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.util.SignatureUtil;
import ru.kopylov.tm.util.TokenUtil;

@Getter
@NoArgsConstructor
public final class SessionService extends AbstractService implements ISessionService {

    @NotNull
    private IPropertyService propertyService = new PropertyService();

    @Nullable
    private ISessionRepository sessionMapper;

    public SessionService(@NotNull ServiceLocator bootstrap, @NotNull IPropertyService propertyService) {
        super(bootstrap);
        this.propertyService = propertyService;
    }

    public boolean remove(@Nullable final String sessionId) {
        if (sessionId == null || sessionId.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            sessionMapper = sqlSession.getMapper(ISessionRepository.class);
            sessionMapper.remove(sessionId);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    public boolean removeByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            sessionMapper = sqlSession.getMapper(ISessionRepository.class);
            sessionMapper.removeByUserId(userId);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    public boolean persist(@Nullable final Session session) {
        if (session == null) return false;
        if (session.getId() == null) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            sessionMapper = sqlSession.getMapper(ISessionRepository.class);
            sessionMapper.persist(session);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    public Session persist(@Nullable final User user) {
        if (user == null) return null;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @Nullable final String sessionSignature = SignatureUtil
                .sign(session, propertyService.getSalt(), propertyService.getCycle());
        session.setSignature(sessionSignature);
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            sessionMapper = sqlSession.getMapper(ISessionRepository.class);
            sessionMapper.persist(session);
            sqlSession.commit();
            return session;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    public Session findOne(@Nullable final String sessionId) {
        if (sessionId == null || sessionId.isEmpty()) return null;
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            sessionMapper = sqlSession.getMapper(ISessionRepository.class);
            return sessionMapper.findOne(sessionId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void validate(@Nullable final Session clientSession) throws Exception {
        if (
                clientSession == null ||
                clientSession.getSignature() == null ||
                clientSession.getTimestamp() == null ||
                clientSession.getUserId() == null
        ) throw new Exception("User is not logged in.");
        @Nullable final Session serverSession = findOne(clientSession.getId());
        if (serverSession == null) throw new Exception("Session does not exist.");
        if (!clientSession.getSignature().equals(serverSession.getSignature()))
            throw new Exception("Session is not valid.");
        clientSession.setSignature(null);
        @Nullable final String clientSignature = SignatureUtil
                .sign(clientSession, propertyService.getSalt(), propertyService.getCycle());
        if (!serverSession.getSignature().equals(clientSignature))
            throw new Exception("Session is not valid.");
        final long timeDifference = System.currentTimeMillis() - clientSession.getTimestamp();
        if (timeDifference > propertyService.getLifeTime()) throw new Exception("Session expired.");
    }

    @Nullable
    public String encryptToken(@Nullable final Session session) throws Exception {
        if (session == null) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String plainJson = objectMapper.writeValueAsString(session);
        return TokenUtil.encrypt(plainJson, propertyService.getSecretKey());
    }

    @Nullable
    public Session decryptToken(@Nullable final String token) throws Exception {
        if (token == null || token.isEmpty()) return null;
        @NotNull String encodedJson = TokenUtil.decrypt(token, propertyService.getSecretKey());
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(encodedJson, Session.class);
        return session;
    }

}
