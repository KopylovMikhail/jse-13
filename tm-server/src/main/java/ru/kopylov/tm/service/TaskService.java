package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.enumerated.TypeSort;
import ru.kopylov.tm.util.DateUtil;
import ru.kopylov.tm.util.TaskComparator;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class TaskService extends AbstractService implements ITaskService {

    @Nullable
    private ITaskRepository taskMapper;

    public TaskService(@NotNull ServiceLocator bootstrap) {
        super(bootstrap);
    }

    public boolean persist(@Nullable final Task task) {
        if (task == null ) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            taskMapper.persist(task);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    public boolean persist(@Nullable final String currentUserId, @Nullable final String taskName) {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setUserId(currentUserId);
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            taskMapper.persist(task);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    public List<Task> findAll() {
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            return taskMapper.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @NotNull
    public List<Task> findAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            return taskMapper.findAllByUserId(currentUserId);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @Nullable
    public List<Task> findAll(@Nullable final String currentUserId, @Nullable final String typeSort) {
        if (currentUserId == null || currentUserId.isEmpty()) return null;
        @NotNull final List<Task> taskList;
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            taskList = taskMapper.findAllByUserId(currentUserId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        if (TypeSort.CREATE_DATE.getDisplayName().equals(typeSort) || typeSort == null || typeSort.isEmpty())
            return taskList;
        if (TypeSort.START_DATE.getDisplayName().equals(typeSort)) {
            taskList.sort(TaskComparator.byDateStart);
            return taskList;
        }
        if (TypeSort.FINISH_DATE.getDisplayName().equals(typeSort)) {
            taskList.sort(TaskComparator.byDateFinish);
            return taskList;
        }
        if (TypeSort.STATE.getDisplayName().equals(typeSort)) {
            taskList.sort(TaskComparator.byState);
            return taskList;
        }
        return null;
    }

    public boolean merge(@Nullable final Task task) {
        if (task == null ) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            taskMapper.merge(task);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    public boolean merge(
            @Nullable final String currentUserId,
            @NotNull final Integer taskNumber,
            @Nullable final String taskName,
            @Nullable final String taskDescription,
            @Nullable final String taskDateStart,
            @Nullable final String taskDateFinish,
            @Nullable final Integer stateNumber
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final List<Task> taskList;
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            taskList = taskMapper.findAllByUserId(currentUserId);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if (taskNumber < 1 || taskNumber > taskList.size()) return false;
        @NotNull final Task task = taskList.get(taskNumber - 1);
        if (taskName != null && !taskName.isEmpty()) task.setName(taskName);
        if (taskDescription != null && !taskDescription.isEmpty())
            task.setDescription(taskDescription);
        if (taskDateStart != null && !taskDateStart.isEmpty()) {
            @NotNull final Date dateStart = DateUtil.stringToDate(taskDateStart);
            task.setDateStart(dateStart);
        }
        if (taskDateFinish != null && !taskDateFinish.isEmpty()) {
            @NotNull final Date dateFinish = DateUtil.stringToDate(taskDateFinish);
            task.setDateFinish(dateFinish);
        }
        if (stateNumber != null) {
            if (stateNumber < 1 || stateNumber > State.values().length) return false;
            task.setState(State.values()[stateNumber-1]);
        }
        return merge(task);
    }

    public boolean remove(@Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            taskMapper.remove(taskId);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    public boolean remove(@Nullable final String currentUserId, @NotNull final Integer taskNumber) {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final List<Task> taskList = findAll(currentUserId);
        if (taskNumber < 1 || taskNumber > taskList.size()) return false;
        @NotNull final Task task = taskList.get(taskNumber - 1);
        return remove(task.getId());
    }

    public void removeAll() {
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            taskMapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    public void removeAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            taskMapper.removeAllByUserId(currentUserId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    public List<Task> findByContent(@Nullable String content) {
        if (content == null || content.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            return taskMapper.findByContent(content);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

}
