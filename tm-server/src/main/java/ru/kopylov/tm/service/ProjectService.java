package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IProjectRepository;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.api.service.IProjectService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.enumerated.TypeSort;
import ru.kopylov.tm.util.DateUtil;
import ru.kopylov.tm.util.ProjectComparator;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class ProjectService extends AbstractService implements IProjectService {

    @Nullable
    private IProjectRepository projectMapper;

    @Nullable
    private ITaskRepository taskMapper;

    public ProjectService(@NotNull ServiceLocator bootstrap) {
        super(bootstrap);
    }

    public boolean persist(@Nullable final Project project) {
        if (project == null) return false;
        if (project.getId() == null) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            projectMapper.persist(project);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    public boolean persist(@Nullable final String currentUserId, @Nullable final String projectName) {
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUserId(currentUserId);
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            projectMapper.persist(project);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    public List<Project> findAll() {
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            return projectMapper.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @NotNull
    public List<Project> findAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            return projectMapper.findAllByUserId(currentUserId);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @Nullable
    public List<Project> findAll(@Nullable final String currentUserId, @Nullable final String typeSort) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        @NotNull final List<Project> projectList;
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            projectList = projectMapper.findAllByUserId(currentUserId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        if (TypeSort.CREATE_DATE.getDisplayName().equals(typeSort) || typeSort == null || typeSort.isEmpty())
            return projectList;
        if (TypeSort.START_DATE.getDisplayName().equals(typeSort)) {
            projectList.sort(ProjectComparator.byDateStart);
            return projectList;
        }
        if (TypeSort.FINISH_DATE.getDisplayName().equals(typeSort)) {
            projectList.sort(ProjectComparator.byDateFinish);
            return projectList;
        }
        if (TypeSort.STATE.getDisplayName().equals(typeSort)) {
            projectList.sort(ProjectComparator.byState);
            return projectList;
        }
        return null;
    }

    public boolean merge(@Nullable final Project project) {
        if (project == null) return false;
        if (project.getId() == null || project.getId().isEmpty()) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            projectMapper.merge(project);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    public boolean merge(
            @Nullable final String currentUserId,
            @NotNull final Integer projectNumber,
            @Nullable final String projectName,
            @Nullable final String projectDescription,
            @Nullable final String projectDateStart,
            @Nullable final String projectDateFinish,
            @Nullable final Integer stateNumber
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final List<Project> projectList;
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            projectList = projectMapper.findAllByUserId(currentUserId);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if (projectNumber < 1 || projectNumber > projectList.size()) return false;
        @NotNull final Project project = projectList.get(projectNumber - 1);
        if (projectName != null && !projectName.isEmpty()) project.setName(projectName);
        if (projectDescription != null && !projectDescription.isEmpty())
            project.setDescription(projectDescription);
        if (projectDateStart != null && !projectDateStart.isEmpty()) {
            @NotNull final Date dateStart = DateUtil.stringToDate(projectDateStart);
            project.setDateStart(dateStart);
        }
        if (projectDateFinish != null && !projectDateFinish.isEmpty()) {
            @NotNull final Date dateFinish = DateUtil.stringToDate(projectDateFinish);
            project.setDateFinish(dateFinish);
        }
        if (stateNumber != null) {
            if (stateNumber < 1 || stateNumber > State.values().length) return false;
            project.setState(State.values()[stateNumber-1]);
        }
        return merge(project);
    }

    public boolean remove(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            projectMapper.remove(projectId);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    public boolean remove(@Nullable final String currentUserId, @NotNull final Integer projectNumber) {
        @NotNull final List<Project> projectList = findAll(currentUserId);
        if (projectNumber < 1 || projectNumber > projectList.size()) return false;
        @NotNull final Project project = projectList.get(projectNumber - 1);
        return remove(project.getId());
    }

    public void removeAll() {
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            projectMapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    public void removeAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            projectMapper.removeAllByUserId(currentUserId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    public boolean setTask(
            @Nullable final String currentUserId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) return false;
        if (taskId == null || taskId.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            taskMapper.setProjectId(currentUserId, projectId, taskId);
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    public boolean setTask(
            @Nullable final String currentUserId,
            @NotNull final Integer projectNumber,
            @NotNull final Integer taskNumber
    ) {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        try {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final List<Project> projectList = projectMapper.findAllByUserId(currentUserId);
            @NotNull final List<Task> taskList = taskMapper.findAllByUserId(currentUserId);
            if (projectNumber < 1 || projectNumber > projectList.size()) return false;
            if (taskNumber < 1 || taskNumber > taskList.size()) return false;
            @NotNull final Project project = projectList.get(projectNumber - 1);
            @NotNull final Task task = taskList.get(taskNumber - 1);
            taskMapper.setProjectId(currentUserId, project.getId(), task.getId());
            sqlSession.commit();
            return true;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return false;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    public List<Task> getTaskList(
            @Nullable final String currentUserId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            taskMapper = sqlSession.getMapper(ITaskRepository.class);
            return taskMapper.findAllByProjectId(projectId);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @NotNull
    public List<Task> getTaskList(
            @Nullable final String currentUserId,
            @NotNull final Integer projectNumber
    ) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        @NotNull final List<Project> projectList;
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            projectList = projectMapper.findAllByUserId(currentUserId);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
        if (projectNumber < 1 || projectNumber > projectList.size()) return Collections.emptyList();
        @NotNull final Project project = projectList.get(projectNumber - 1);
        return getTaskList(currentUserId, project.getId());
    }

    @NotNull
    public List<Project> findByContent(@Nullable String content) {
        if (content == null || content.isEmpty()) return Collections.emptyList();
        try (@NotNull final SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession()) {
            projectMapper = sqlSession.getMapper(IProjectRepository.class);
            return projectMapper.findByContent(content);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

}
